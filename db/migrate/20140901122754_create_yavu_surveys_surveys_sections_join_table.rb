class CreateYavuSurveysSurveysSectionsJoinTable < ActiveRecord::Migration
  def change
     create_join_table :yavu_surveys_surveys, :sections do |t|
       t.index [:yavu_surveys_survey_id, :section_id], unique: true, name: 'yavu_surveys_survey_id_section_id'
     end
  end
end
