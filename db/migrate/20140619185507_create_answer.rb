class CreateAnswer < ActiveRecord::Migration
  def change
    create_table :yavu_surveys_answers do |t|
      t.text :title, null: false
      t.integer :base_value, null: false, default: 0

      t.belongs_to :survey, index: true
    end
  end
end
