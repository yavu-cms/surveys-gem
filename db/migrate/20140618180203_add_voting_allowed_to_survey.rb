class AddVotingAllowedToSurvey < ActiveRecord::Migration
  def change
    add_column :yavu_surveys_surveys, :voting_allowed, :boolean, default: true
  end
end
