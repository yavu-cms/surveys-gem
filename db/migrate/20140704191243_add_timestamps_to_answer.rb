class AddTimestampsToAnswer < ActiveRecord::Migration
  def change
    add_column :yavu_surveys_answers, :created_at, :datetime
    add_column :yavu_surveys_answers, :updated_at, :datetime
  end
end
