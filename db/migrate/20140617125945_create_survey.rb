class CreateSurvey < ActiveRecord::Migration
  def change
    create_table :yavu_surveys_surveys do |t|
      t.text :question, null: false
      t.date :date, null: false
    end
  end
end
