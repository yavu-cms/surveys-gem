class AddTimestampsToSurvey < ActiveRecord::Migration
  def change
    add_column :yavu_surveys_surveys, :created_at, :datetime
    add_column :yavu_surveys_surveys, :updated_at, :datetime
  end
end
