class AddActiveToSurvey < ActiveRecord::Migration
  def change
    add_column :yavu_surveys_surveys, :active, :boolean, default: true
  end
end
