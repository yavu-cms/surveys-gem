class RemoveDateFromSurvey < ActiveRecord::Migration
  def change
    remove_column :yavu_surveys_surveys, :date, :datetime
  end
end
