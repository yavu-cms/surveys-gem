mongoid_config = <<-yaml
sessions:
  default:
    database: yavu_surveys_test
    hosts:
      - localhost:27017
    options:
      max_retries: 0
      retry_interval: 0
yaml

Mongoid.load_configuration YAML.load(mongoid_config)
