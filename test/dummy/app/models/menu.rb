class Menu
  cattr_accessor :extras do
    []
  end

  def self.extras?
    extras.any?
  end

  def value;   end
  def path;    end
  def options; end
end
