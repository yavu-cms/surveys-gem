# This migration comes from yavu_surveys (originally 20140617125945)
class CreateSurvey < ActiveRecord::Migration
  def change
    create_table :yavu_surveys_surveys do |t|
      t.text :question, null: false
      t.date :date, null: false
    end
  end
end
