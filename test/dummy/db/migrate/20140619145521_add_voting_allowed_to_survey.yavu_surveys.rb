# This migration comes from yavu_surveys (originally 20140618180203)
class AddVotingAllowedToSurvey < ActiveRecord::Migration
  def change
    add_column :yavu_surveys_surveys, :voting_allowed, :boolean, default: true
  end
end
