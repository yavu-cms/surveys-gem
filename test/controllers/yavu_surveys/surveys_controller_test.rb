require 'test_helper'

module YavuSurveys
  class SurveysControllerTest < ActionController::TestCase
    describe SurveysController do
      before do
        @controller = SurveysController.new
        @survey = Survey.new(question: '???', answers: [ Answer.new(title: 'Yes') ])
      end
      after do
        # Erase all residual data from Mongo models
        Vote.delete_all
      end

      describe 'get index' do
        describe 'on a regular request' do
          it 'must succeed' do
            get :index, use_route: :yavu_surveys
            must_render :index
            assigns(:surveys).wont_be_nil
          end
        end
      end

      describe 'get new' do
        it 'must succeed' do
          get :new, use_route: :yavu_surveys
          must_render :new
          assigns(:survey).must_be :new_record?
        end
      end

      describe 'post create' do
        describe 'when valid data is posted' do
          it 'adds an survey and redirects to the index page' do
            skip
          end
        end

        describe 'when invalid data is posted' do
          it 'shows again the new page and displays the validation errors' do
            skip
          end
        end
      end

      describe 'get edit' do
        before do
          @survey.save
        end
        it 'must succeed' do
          get :edit, id: @survey.id, use_route: :yavu_surveys
          must_render :edit
          assigns(:survey).wont_be :new_record?
        end
      end

      describe 'patch update' do
        describe 'when a valid set of attributes is submitted' do
          it 'updates the survey and redirects to index' do
            skip
          end
        end

        describe 'when an invalid set of attributes is submitted' do
          it 'shows again the edit page and displays the validation errors' do
            skip
          end
        end
      end

      describe 'delete destroy' do
        before do
          @survey.save
        end
        it 'deletes the survey and redirects to the index page' do
          delete :destroy, id: @survey.id, use_route: :yavu_surveys
          Survey.all.to_a.must_be :empty?
          # must_redirect_to yavu_surveys_surveys_path
        end

        it 'wont delete the survey if is used in a survey component' do
          skip
        end
      end
    end
  end
end
