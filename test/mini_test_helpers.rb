# Helper methods specific to MiniTest
module MiniTestHelpers
  def login(user = nil)
    user ||= create(:admin)
    session['User'] = user.id
  end

  def authenticated
    User.find(session[User.to_s]) if session[User.to_s]
  end

  def create_route_with_component(route: route_factory, component: component)
    component_configuration = build(:component_configuration, component: component)
    representation = build(:representation_with_component, component_configuration: component_configuration)
    create(route, representation: representation)
  end


  def db_alteration_procedure(op, diff, model, factory, options, session)
    sym = model.to_s.underscore.to_sym
    factory ||= sym
    case op
      when :destroy
        if factory.is_a? model
          object_to_delete = factory
        else
          object_to_delete = create factory
        end
        count = model.count
        options[:id] = object_to_delete
        delete :destroy, options, session
      when :create
        count = model.count
        options = { sym => attributes_for(factory) }.deep_merge(options)
        post :create, options, session
      else
        raise "Unknown db alter operation: #{op.inspect}"
    end
    expected_count = [count + diff, 0].max
    model.count.must_equal expected_count
  end

  # Assertion for controller tests that checks if a POST request
  # to the +create+ action for a given +model+ actually increases
  # in one the count of objects for that model.
  #
  # === Examples
  #
  #   it 'adds a Supplement' do
  #     it_adds_a Supplement
  #   end
  #
  #   it 'adds an Edition' do
  #     it_adds_an Edition, :edition_for_tomorrow
  #   end
  def it_adds_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :create, 1, model, factory, options, session
  end
  alias :it_adds_an :it_adds_a

  # Negative assertion
  def it_doesnt_add_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :create, 0, model, factory, options, session
  end
  alias :it_doesnt_add_an :it_doesnt_add_a

  # Assertion for controller tests that checks if a DELETE request
  # to the +destroy+ action for a given +model+ actually decreases
  # in one the count of objects for that model.
  #
  # === Examples
  #
  #   it 'deletes a Tag' do
  #     it_deletes_a Tag
  #   end
  #
  #   it 'deletes an Article' do
  #     it_deletes_an Article, :article_with_title
  #   end
  def it_deletes_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :destroy, -1, model, factory, options, session
  end
  alias :it_deletes_an :it_deletes_a

  # Negative assertion
  def it_doesnt_delete_a(model, factory: nil, options: {}, session: {})
    db_alteration_procedure :destroy, 0, model, factory, options, session
  end
  alias :it_doesnt_delete_an :it_doesnt_delete_a

  def must_redirect_to(url, allow_query_params = false)
    [200, 302].must_include response.status
    response.location.must_match %r(#{url}#{'\\?.*' if allow_query_params}\z)
  end

  # Assertion for controller tests that checks if the last request
  # succeeded and rendered the expected template.
  # Parameters for this method are provided as received to the
  # +assert_template+ assertion.
  #
  # === Examples
  #
  #   it 'renders the index view' do
  #     must_render :index
  #   end
  #
  # @see TemplateAssertions#assert_template
  def must_render(options = {}, message = nil)
    must_succeed
    assert_template options, message
  end

  # Assertion to check if the form errors are being displayed on the page.
  #
  # === Examples
  #
  #   it 'fails to validate' do
  #     must_display_form_errors
  #   end
  def must_display_form_errors
    assert_select '#error_explanation .error-msg'
  end

  # Assertion to check if a flash message is set for a type with a
  # specific message. Note that +message+ should be the i18n key that
  # will be used for the message and that this helper will automatically
  # translate it using I18n#translate to check if it is correctly set.
  #
  # === Examples
  #
  #   it 'sets a nice notice' do
  #     must_have_flash_message :notice, 'articles.clone.notice'
  #   end
  def must_have_flash_message(type, message, placeholders)
    flash[type].must_equal I18n.translate(message, placeholders)
  end

  # Handy method to check if a notice flash message is set.
  #
  # === Examples
  #
  #   it 'sets a nice notice' do
  #     must_have_notice 'articles.clone.notice'
  #   end
  #
  #   # Or with some placeholders for translation:
  #
  #   it 'sets a nice notice' do
  #     must_have_notice 'articles.clone.notice', article_name: article.name
  #   end
  def must_have_notice(message, placeholders = {})
    must_have_flash_message :notice, message, placeholders
  end

  # Handy method to check if an info flash message is set.
  #
  # === Examples
  #
  #   it 'sets a great info' do
  #     must_have_info 'representations.cover.info'
  #   end
  #
  #   # Or with some placeholders for translation:
  #
  #   it 'sets a great info' do
  #     must_have_info 'representations.cover.info', representation_name: representation.name
  #   end
  def must_have_info(message, placeholders = {})
    must_have_flash_message :info, message, placeholders
  end

  # Handy method to check if an alert flash message is set.
  #
  # === Examples
  #
  #   it 'sets a horrible alert' do
  #     must_have_alert 'articles.clone.alert'
  #   end
  #
  #   # Or with some placeholders for translation:
  #
  #   it 'sets a horrible alert' do
  #     must_have_alert 'articles.clone.alert', article_name: article.name
  #   end
  def must_have_alert(message, placeholders = {})
    must_have_flash_message :alert, message, placeholders
  end

  def must_succeed
    assert_response :success
    # TO BE ADDED SOON ~.~
    # assert_select '.translation_missing', false, 'Found missing translations'
  end
end
