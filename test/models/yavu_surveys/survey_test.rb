require 'test_helper'

module YavuSurveys
  class SurveyTest < ActiveSupport::TestCase
    before do
      a1 = Answer.new title: 'Yes', base_value: 250
      a2 = Answer.new title: 'No',  base_value: 500
      @survey = Survey.new question: 'Question',
        date: Date.today,
        answers: [ a1, a2 ]
    end

    describe 'validations' do
      describe 'when all fields are present' do
        it 'is valid' do
          @survey.must_be :valid?
        end
      end

      describe 'question' do
        describe 'has not?' do
          it 'must fail' do
            @survey.question = ""
            @survey.wont_be :valid?
            @survey.errors.wont_be :empty?
            @survey.errors.must_include :question
          end
        end
      end

      describe 'answers' do
        describe 'has not?' do
          it 'must fail' do
            @survey.answers = []
            @survey.wont_be :valid?
            @survey.errors.wont_be :empty?
            @survey.errors.must_include :answers
          end
        end
        describe 'there are errors in some of contained answers' do
          it 'must fail' do
            @survey.answers.first[:title] = ''
            @survey.wont_be :valid?
            @survey.errors.wont_be :empty?
            @survey.errors.must_include :"answers.title"
          end
        end
      end
    end

    describe '#vote_for' do
      before do
        @answer = @survey.answers.first
      end
      describe 'when all favorable conditions are given' do
        it 'must registers a vote and returns true' do
          @answer.expects(:vote).with('Charles').once
          @survey.vote_for(@answer, 'Charles').must_equal true
        end
      end
      describe 'when answer does not correspond to this survey' do
        before do
          @another_answer = Answer.new title: 'Another'
        end
        it 'returns false' do
          @survey.vote_for(@another_answer, 'Charles').must_equal false
        end
      end
      describe 'when user has voted' do
        before do
          @survey.expects(:has_voted?).with('Charles').returns(true)
        end
        it 'returns false' do
          @survey.vote_for(@answer, 'Charles').must_equal false
        end
      end
      describe 'when voting are disabled' do
        before do
          @survey.voting_allowed = false
        end
        it 'returns false' do
          @survey.vote_for(@answer, 'Charles').must_equal false
        end
      end
    end
  end
end


