require 'test_helper'

module YavuSurveys
  class VoteTest < ActiveSupport::TestCase
    before do
      # This object must be built to be valid
      @vote = Vote.new user: 'Charles',
        answer: Answer.new(title: 'Yes', survey:
          Survey.new(question: 'Survey'))
    end
    after do
      Vote.delete_all
    end

    describe 'validations' do
      describe 'when all fields are present' do
        it 'is valid' do
          @vote.must_be :valid?
        end
      end

      describe 'user' do
        describe 'has not?' do
          it 'must fail' do
            @vote.user = ''
            @vote.wont_be :valid?
            @vote.errors.wont_be :empty?
            @vote.errors.must_include :user
          end
        end
      end

      describe 'answer' do
        describe 'has not?' do
          it 'must fail' do
            @vote.answer = nil
            @vote.wont_be :valid?
            @vote.errors.wont_be :empty?
            @vote.errors.must_include :answer
          end
        end
        describe 'is invalid?' do
          # Reason: http://mongoid.org/en/mongoid/docs/validation.html
          it 'will success' do
            # answer invalid because it has no title
            a = Answer.new
            @vote.answer = a
            @vote.must_be :valid?
          end
        end
      end
    end

    describe 'interaction with MongoDB' do
      after do
        Answer.delete_all
      end
      describe '.from 'do
        before do
          @a1 = Answer.create(title: 'No')
          @a2 = Answer.create(title: 'Yes')
          Vote.create(answer: @a1.id, user: 'Charles')
          Vote.create(answer: @a2.id, user: 'David')
          Vote.create(answer: @a2.id, user: 'Andrew')
          Vote.create(answer: @a1.id, user: 'Clara')
          Vote.create(answer: @a1.id, user: 'Alice')
        end
        it 'must return all votes from an answer' do
          Vote.from(@a1).map(&:user).must_equal %w(Charles Clara Alice)
        end
      end
      describe '.add 'do
        before do
          @a1 = Answer.create(title: 'No')
        end
        it 'must create votes of certain answer by an user' do
          Vote.add @a1, 'Charles'
          Vote.add @a1, 'David'
          Vote.add @a1, 'Alice'
          Vote.all.to_a.map(&:user).must_equal %w(Charles David Alice)
        end
      end
      describe '.exists? 'do
        before do
          @a1 = Answer.create(title: 'Yes')
          @a2 = Answer.create(title: 'No')
          Vote.create(answer: @a1.id, user: 'Charles')
        end
        it 'determines if an user voted in certain answer' do
          Vote.exists?(@a1, 'Charles').must_equal true
          Vote.exists?(@a1, 'Simon').must_equal false
          Vote.exists?(@a2, 'Charles').must_equal false
        end
      end
    end
  end
end
