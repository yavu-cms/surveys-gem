require 'test_helper'

module YavuSurveys
  class AnswerTest < ActiveSupport::TestCase
    before do
      # This object must be built to be valid
      @answer = Answer.new title: 'Yes',
        survey: Survey.new(question: 'Survey')
    end

    describe 'validations' do
      describe 'when all fields are present' do
        it 'is valid' do
          @answer.must_be :valid?
        end
      end

      describe 'title' do
        describe 'has not?' do
          it 'must fail' do
            @answer.title = ''
            @answer.wont_be :valid?
            @answer.errors.wont_be :empty?
            @answer.errors.must_include :title
          end
        end
      end
    end

    describe "#vote_count" do
      before do
        @answer.base_value = 100
        @answer.expects(:votes).returns(Array.new(10, mock))
      end
      it "returns all votes when *add_base* parameter is true (by omission, is true)" do
        @answer.vote_count.must_equal 110
      end
      it "returns only real user votes when *add_base* parameter is false" do
        @answer.vote_count(add_base: false).must_equal 10
      end
    end
    describe "#voted?" do
      it "need a simple test"
    end
    describe "#vote" do
      it "need a simple test"
    end
    describe "#votes" do
      it "need a simple test"
    end
  end
end


