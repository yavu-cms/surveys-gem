require_dependency "yavu_surveys/application_controller"

module YavuSurveys
  class SurveysController < ApplicationController
    before_action :set_survey, only: [ :edit, :update, :destroy, :enable, :disable ]

    def index
      @surveys = Survey.all
    end

    def search
      query = params[:q]
      matches = Survey.search query
      render json: matches.map { |survey| Hash[id: survey.id.to_s, text: survey.question] }
    end

    def new
      @survey = Survey.new
    end

    def edit
      render layout: !request.xhr?
    end

    def create
      @survey = Survey.new survey_params
      if @survey.save
        redirect_to surveys_path, notice: I18n.t('yavu_surveys/surveys.new.messages.success')
      else
        render action: 'new', layout: !request.xhr?
      end
    end

    def update
      if @survey.update(survey_params)
        redirect_to surveys_path, notice: I18n.t('yavu_surveys/surveys.edit.messages.success')
      else
        render action: 'edit', layout: !request.xhr?
      end
    end

    def enable
      @survey.update(active: true)
      redirect_to surveys_path, notice: I18n.t('yavu_surveys/surveys.enable.messages.success')
    end

    def disable
      @survey.update(active: false)
      redirect_to surveys_path, notice: I18n.t('yavu_surveys/surveys.disable.messages.success')
    end

    def destroy
      message = if @survey.destroy
        I18n.t('yavu_surveys/surveys.destroy.messages.success')
      else
        I18n.t('yavu_surveys/surveys.destroy.messages.failure')
      end
      redirect_to surveys_path, notice: message
    end

    private

    def survey_params
      params.require(:survey).permit(:question, :voting_allowed, :active, section_ids: [], answers_attributes: [:id, :title, :base_value, :_destroy])
    end

    def set_survey
      @survey = Survey.find(params[:id])
    end
  end
end
