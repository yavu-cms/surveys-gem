module YavuSurveys
  class Vote
    include Mongoid::Document

    field :a, as: :answer, type: Integer
    field :u, as: :user, type: String

    index a: -1
    index u:  1

    validates_presence_of :answer
    validates_presence_of :user

    def self.from(answer)
      where(answer: key_field_for(answer))
    end

    def self.add(answer, user)
      create(answer: key_field_for(answer), user: key_field_for(user))
    end

    def self.exists?(answer, user)
      where(answer: key_field_for(answer), user: key_field_for(user)).exists?
    end

    private

    def self.key_field_for(object)
      if object.is_a? String
        object
      else
        object.id
      end
    end
  end
end
