module YavuSurveys
  class Answer < ActiveRecord::Base
    belongs_to :survey

    validates :title, presence: true
    validates :base_value, presence: true

    # Attributes to be exported for API
    set_api_resource id: :id,
      title: :title,
      base_value: :base_value,
      survey: ->(_) { survey.id },
      vote_count: ->(_) { vote_count }

    def voted?(user)
      Vote.exists?(self, user)
    end

    def vote_count(add_base: true)
      votes.size + (add_base ? base_value : 0)
    end

    def vote(user)
      Vote.add self, user
    end

    def votes
      Vote.from self
    end
  end
end
