module YavuSurveys
  class Survey < ActiveRecord::Base
    has_many :answers
    has_and_belongs_to_many :sections, foreign_key: 'yavu_surveys_survey_id'
    accepts_nested_attributes_for :answers, allow_destroy: true

    validates :question, presence: true
    validates :answers, presence: true
    validates :sections, presence: true

    after_touch :touch_answers

    default_scope ->{ order(created_at: :desc) }

    def self.active(in_section: nil)
      Rails.cache.fetch("active", expires: YavuSettings.cache.backend.expiration_time.minutes) do
        if in_section
          includes(:sections).where('sections.id' => in_section.id, :active => true)
        else
          where(active: true)
        end.first
      end
    end

    # Attributes to be exported for API
    set_api_resource id: :question,
      question: :question,
      date: :date,
      voting_allowed: :voting_allowed,
      total_votes: :total_votes,
      answers: :answers

    def has_voted?(user)
      answers.any? { |a| a.voted? user }
    end

    def self.search(q)
      where 'question LIKE :query', query: "%#{q}%"
    end

    def total_votes
      answers.inject(0) { |total, answer| total + answer.vote_count }
    end

    def vote_for(answer, user)
      if answers.include?(answer) && !has_voted?(user) && active && voting_allowed
        answer.vote(user); touch; true
      else
        false
      end
    end

    def visible?
      true
    end

    def date
      created_at
    end

    def to_s
      question
    end

    protected

    def touch_answers
      answers.each &:touch
    end
  end
end
