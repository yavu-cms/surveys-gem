module YavuSurveys
  module ApplicationHelper
    def title(content = nil, &block)
      content = capture(&block) if block_given?
      content_for :title, content.html_safe
      content
    end

    def render_flashes
      render partial: 'shared/flashes' if flash.any?
    end

    def render_form_errors(source)
      render(partial: "shared/form_errors", locals: { errors: source.errors, model_name: t("mongoid.models.#{source.class.name.to_s.downcase}")}) unless source.errors.nil?
    end

    def date_with_format(datetime, date_format = :default)
      I18n.l datetime.to_date, format: date_format unless datetime.nil?
    end

    def time_with_format(datetime, time_format = :default)
      I18n.l datetime.to_time, format: time_format unless datetime.nil?
    end
  end
end
