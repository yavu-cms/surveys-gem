YavuSurveys::Engine.routes.draw do
  resources :surveys do
    member do
      get 'enable'
      get 'disable'
    end
    collection do
      get 'search'
    end
  end
end
