$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "yavu_surveys/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "yavu_surveys"
  s.version     = YavuSurveys::VERSION
  s.authors     = ["CeSPI"]
  s.email       = ["desarrollo@cespi.unlp.edu.ar"]
  s.homepage    = ""
  s.summary     = "YavuSurveys is a surveys module"
  s.description = "YavuSurveys is a surveys module"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_development_dependency "cespigems", "~> 0.1"
  s.add_development_dependency "sqlite3"
  s.add_development_dependency "mocha", "~> 0.14.0"

  s.add_dependency "rails", "~> 4.0.13"
  s.add_dependency "mongoid", "~> 4.0.0"
  s.add_dependency "nested_form", "~> 0.3.2"
  s.add_dependency "yavu-api-resource", "~> 1.0"
end
