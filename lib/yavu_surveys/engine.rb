module YavuSurveys
  class Engine < ::Rails::Engine
    require 'nested_form'

    isolate_namespace YavuSurveys

    # Bring up our proper and isolated traslations
    config.i18n.load_path += Dir[config.root.join('locales', '*.yml').to_s]

    # Inject dependencies to main app
    initializer :add_survey_to_component_field do |app|
      ComponentField.model_dict['yavusurveys-survey'] = Survey
      ComponentsHelper.autocomplete_fields['yavusurveys-survey'] = ->(object,*args) { object.try(:yavu_surveys).try(:search_surveys_path,*args) }
    end

    initializer :add_surveys_to_extras do |app|
      Menu.extras << Class.new(Menu) do
        include YavuSurveys::Engine.routes.url_helpers

        def value
          I18n.t('yavu_surveys.menu.surveys_name')
        end
        def path
          surveys_path
        end
      end.new
    end

    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end
  end
end
